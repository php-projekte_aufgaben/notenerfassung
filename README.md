# Notenerfassung 2.0

# Inhaltsverzeichnis

- [Notenerfassung 2.0](#notenerfassung-20)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Ausgangslage](#ausgangslage)
  - [Anforderungen](#anforderungen)
  - [Mockup](#mockup)
  - [Lösungsansatz](#lösungsansatz)
  - [Codeimplementierung und Umsetzung](#codeimplementierung-und-umsetzung)
  - [Testprotokoll](#testprotokoll)
- [Theorie](#theorie)
  - [Cookies](#cookies)
    - [Cookie erstellen](#cookie-erstellen)
    - [Cookie abrufen](#cookie-abrufen)
    - [Cookie löschen](#cookie-löschen)
    - [Überprüfen ob Cookies aktiviert sind](#überprüfen-ob-cookies-aktiviert-sind)
  - [PHP Sessions](#php-sessions)
    - [Was ist eine PHP-Sitzung?](#was-ist-eine-php-sitzung)
    - [Starten einer PHP-Sitzung](#starten-einer-php-sitzung)
    - [Wert der PHP-Sitzungsvariablen abrufen](#wert-der-php-sitzungsvariablen-abrufen)
    - [Ändern einer PHP-Sitzungsvariable](#ändern-einer-php-sitzungsvariable)
    - [Eine PHP-Sitzung zerstören](#eine-php-sitzung-zerstören)
  - [Objektorientierung](#objektorientierung)
    - [Was ist Objektorientierte-Programmierung?](#was-ist-objektorientierte-programmierung)
    - [Was sind Klassen und Objekte?](#was-sind-klassen-und-objekte)
    - [Eine Klasse definierten](#eine-klasse-definierten)
    - [Objekte definieren](#objekte-definieren)
    - [Das Schlüsselwort $this](#das-schlüsselwort-this)
    - [Instanz von](#instanz-von)
    - [Konstruktor](#konstruktor)
    - [Destruktor](#destruktor)
    - [Zugriffsmodifikatoren](#zugriffsmodifikatoren)
    - [Vererbung](#vererbung)
    - [Klassenkonstanten](#klassenkonstanten)
    - [Abstrakte Klassen](#abstrakte-klassen)
    - [Schnittstellen](#schnittstellen)
    - [Schnittstellen verwenden](#schnittstellen-verwenden)
    - [Was sind Eigenschaften?](#was-sind-eigenschaften)
    - [Statische Methoden](#statische-methoden)
  - [JSON](#json)
    - [Verwendung von JSON](#verwendung-von-json)
    - [Syntax](#syntax)
    - [Values](#values)
    - [JavaScript-Objekte](#javascript-objekte)

## Ausgangslage

Weiterentwicklung des bisher erstellten Prototypen für die Notenerfassung. 
Er soll nun auch Noten temporär serverseitig abspeichern ,auflisten und löschen können. 
Die Applikation soll von allen Lehrpersonen mit einem herkömmlichen Browser oder einem Mobilgerät verwendet werden können. 

## Anforderungen 

Der PHP-Prototyp sollte mindestens folgendes umfassen:

- Eingabe und Validierung der Noten
- Temporäre serverseitige Speicherung der Daten 
- Löschfunktion um alle Noten zu löschen
- Vollständige Trennung von Darstellung und Geschäftslogik
- Model-Klasse für die Noteneintragung
    - Beinhaltet alle notewendigen Datenfelder und Methoden
    - Beinhaltet die gesamte Logik (Validierung, Speicherung, ...)
- Responsive Design
- Verwendung eines HTML/CSS Frameworks, z.B. Bootstrap

## Mockup

![Mockup-Notenerfassung2.0](images/Mockup.png)

## Lösungsansatz

1. Model-Klasse GradeEntry
   - Private Datenfelder/Instanzvariablen für alle Formularelemente (Errors-Array, Name, E-Mail, Grade, ...)
   - Default-Konstruktor
   - Getter/Setter-Methoden
   - Methoden zu der serverseitigen Validierung (identisch zur ersten Version, keine Parameter, Verwendung der Datenfelder)
   - Methode save
   - Klassenmethode getAll
   - Klassenmethode deleteAll
   - Hilfsmethoden zur formatierten Ausgabe (Fach, Datum, ...)
2. Anpassung der Formularverarbeitung im index.php 
   - Skript wichtig: session_start aufrufen
3. Darstellung der gespeicherten Noten
   - Laden der Daten mit der Klassenmehtode getAll und Darstellung mittels foreach-Schleife in Tabellenform.
4. Löschen-Formular erstellen
   - Auf der Startseite ein weiteres HTML-Formular für das Löschen aller gespeicherten Daten einbauen.
   Die Action sollte hier auf ein eigenes Skript clear.php verweisen. Dies ermöglicht eine saubere Trennung der beiden Formulare. Beim Verarbeiten des Formulares wird die Klassenmethode deleteAll aufgerufen und ein redirect auf index.php durchgeführt. 
<br><br>

![UML-Diagramm-Notenerfassung2.0](images/Diagramm.png)
<br><br> 

## Codeimplementierung und Umsetzung

Erstellung eines neuen Packages models mit der Klasse GradeEntry und deren Datenfelder, Konstruktor und autogenerierten Getter/Setter-Methoden.
```php
class GradeEntry
{
    private $name = '';
    private $email = '';
    private $examDate = '';
    private $subject = '';
    private $grade = '';

    private $errors = [];

    public function __construct(){

    }
```
 <br>
Implementierung der Funktionen zum speichern und validieren. 
Die Funktionen zur Validierung können aus der func.inc.php übernommen werden. 

getAll und deleteAll sind static weil kein einzelnes Notenelement erzeugt werden soll, sondern über die Klasse darauf zugegriffen wird.<br>

Funktion um $_Session-Array auszulesen und die Textrepresentationen in einzelne Objekte zurück zu verwandeln. 
Die Durchführung der Umwandlung erfolgt mit Hilfe von unserialize
```php
public static function getAll(){

        $grades = [];

        if(isset($_SESSION['grades'])){
            foreach ($_SESSION['grades'] as $g){
                $grades[] = unserialize($g);
            }
        }
        return $grades;
    }
```
<br>

Funktion um alle Daten zu löschen. 
Es wird geprüft ob Noten gespeichert sind. Wenn ja wird die unset Funktion aufgerufen um die Session zu beenden.
```php
    public static function deleteAll(){

        if(isset($_SESSION['grades'])){
            unset($_SESSION['grades']);
        }
    }
```
<br>

Funktion um die eingegebenen Daten zu speichern. Überprüft mit der validate() Funktion ob die vom User eingegebenen Daten korrekt sind. 

Die Daten werden temporär serverseitig gespeichert. Was bedeutet, dass sie in einer Session abgespeichert werden. 

Mit serialize wird das GradeEntry-Objekt mit den Instanzvariablen in eine serielle Textrepresentation umgewandelt und in der Variable $s abgespeichert. 

Dem Session-Array wird eine Array-Push-Funktion hinzugefügt $_SESSION['grades'][] um eine neue Noteninstanz an unser Session-Array anzuhägen.
```php
    public function save(){
        if($this->validate()){
            //speichern
            $s = serialize($this);
            $_SESSION['grades'][] = $s;
            return true;
        }
        return false;
    }
```
<br>

Von den übernommenen Funktionen werden die Parameter gelöscht und durch Instanzvariablen(z.B. $this->name, $this->errors) ersetzt.
```php
private function validateName()
    {
        if (strlen($this->name) == 0) {
            $this->errors['name'] = "Name darf nicht leer sein";
            return false;
        } else if (strlen($this->name) > 20) {
            $this->errors['name'] = "Name zu lang";
            return false;
        } else {
            return true;
        }
    }
```
<br> 

Hilfsfunktionen zum Formatieren des Datums und Faches. 
```php
public function getExamDateFormatted(){
        return date_format(date_create($this->examDate), "d.m.Y");
    }

public function getSubjectFormatted(){
    switch ($this->subject){
        case 'm':
            return 'Mathematik';
        case 'd':
            return 'Deutsch';
        case 'e':
            return 'Englisch';
        default:
            return null;
        }
    }
```
<br>

Hilfmethode zum überprüfen ob ein Fehler auftritt.
```php
public function hasErrors($field){
        return isset($this->errors['field']);
    }
```
<br>

Um eine bessere Trennung zwischen HTML Code und der Formularverarbeitung zu erreichen muss die index.php angepasst werden. 

Da mit Session gearbeitet wird, muss diese am Anfang der index.php gestartet werden und anschließend die neue Klasse eingebunden werden. 
```php
session_start();

require_once "models/GradeEntry.php";
``` 
<br>

Erstellung eines neuen GradeEntry-Objektes mit Standart-Konstruktor und Befüllung der Instanzvariablen mit den Werten aus dem Formular. 

War das Validieren der Daten erfolgreich wird es in der Session gespeichert, ansonsten folgt eine Fehlermeldung.
```php
$e = new GradeEntry();
$message = '';

// Formularverarbeitung (HTTP POST Request)
if (isset($_POST['submit'])) {

    // double-check: zuerst pruefen ob die Daten im Request enthalten sind, dann auslesen

    $e->setName(isset($_POST['name']) ? $_POST['name'] : "");
    $e->setEmail(isset($_POST['email']) ? $_POST['email'] : "");
    $e->setExamDate(isset($_POST['examDate']) ? $_POST['examDate'] : "");
    $e->setGrade(isset($_POST['grade']) ? $_POST['grade'] : "");
    $e->setSubject(isset($_POST['subject']) ? $_POST['subject'] : "");

    // Validierung der Daten und Ausgabe des Ergebnisses (an der aktuellen Stelle in der HTML-Seite)
    if ($e->validate()) {
        $e->save();
        $message = "<p class='alert alert-success'>Die eingegebenen Daten sind in Ordnung!</p>";
    } else {
        $message = "<div class='alert alert-danger'><p>Die eingegebenen Daten sind fehlerhaft!</p><ul>";
        foreach ($e->getErrors() as $key => $value) {
            $message .= "<li>" . $value . "</li>";
        }
        $message .= "</ul></div>";
    }
}
```
<br>

Implementierung einer neuen Tabelle für die Notenausgabe in welcher die Daten dynamisch aus der Session geladen werden. 

Über getAll werden alle Noten als Objekte in einem Array abgespeichert und anschließend über eine foreach iteriert und ausgegeben werden.
```php
$grades = GradeEntry::getAll();

            foreach ($grades as $g){
                echo "<tr>";
                echo "<td>" . $g->getName() . "</td>";
                echo "<td>" . $g->getEmail() . "</td>";
                echo "<td>" . $g->getExamDateFormatted() . "</td>";
                echo "<td>" . $g->getSubjectFormatted() . "</td>";
                echo "<td>" . $g->getGrade() . "</td>";
                echo"</tr>";
```
<br>

Formular zum löschen der Daten, falls Noten eingetragen worden sind. 
```php
if (count($grades) > 0){
    ?>
    <form action="clear.php" method="post">
        <input type="submit" name="clear" class="btn btn-danger" value="Alle Noten löschen"/>
    </form>
    <?php
    }
```
<br>

Skript zum löschen der Daten. 
```php
session_start();

if(isset($_POST['clear'])){
    require_once "models/GradeEntry.php";
    GradeEntry::deleteAll();

    header("Location: index.php");
} else {
    http_response_code(405);
```
<br> 

## Testprotokoll 
<br>

![Testbild1](images/Test1.png) 
![Testbild2](images/Bild2.png) 

Unterschiedliche Session-IDs 
![Testbild3](images/Bild3.png)
![Testbild4](images/Bild4.png)
<br><br>

# Theorie 

## Cookies 

Ein Cookie wird häufig verwendet, um einen Benutzer zu identifizieren. Ein Cookie ist eine kleine Datei, die der Server auf dem Computer des Benutzers einbettet. Jedes Mal, wenn derselbe Computer eine Seite mit einem Browser anfordert, sendet er auch das Cookie.

### Cookie erstellen 

Es wird ein Cookie namens "user" mit dem Wert "John Doe" erstellt. Das Cookie verfällt nach 30 Tagen. Das "/" bedeutet, dass das Cookie auf der gesamten Website verfügbar ist (ansonsten wähle das gewünschte Verzeihnis.)

```php
setcookie(name, value, expire, path, domain, secure, httponly);
```
Nur der Parameter name ist erforderlich. Alle anderen Parameter sind optional. 


```php
$cookie_name = "user";
$cookie_value = "John Doe";
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");// 86400 = 1 day
```
### Cookie abrufen

Der Wert des Cookies "user" überprüft ob er nicht vorhanden ist. Ist dies der Fall wird ein Text mit dem Inhalt "Cookie named '" . $cookie_name . "' is not set!" ausgegeben. 
Sollte doch ein Cookie vorhanden sein, so wird der Text "Cookie '" . $cookie_name . "' is set! und der Wert des Cookies wird ausgegeben. 

```php
if(!isset($_COOKIE[$cookie_name])) {
  echo "Cookie named '" . $cookie_name . "' is not set!";
} else {
  echo "Cookie '" . $cookie_name . "' is set!<br>";
  echo "Value is: " . $_COOKIE[$cookie_name];
}
```

### Cookie löschen 

Um ein Cookie zu löschen, verwenden Sie die setcookie()Funktion mit einem Ablaufdatum in der Vergangenheit: 

```php
// set the expiration date to one hour ago
setcookie("user", "", time() - 3600);
``` 

### Überprüfen ob Cookies aktiviert sind 

```php
setcookie("test_cookie", "test", time() + 3600, '/');


if(count($_COOKIE) > 0) {
  echo "Cookies are enabled.";
} else {
  echo "Cookies are disabled.";
}
```
## PHP Sessions

Eine Session ist ein Möglichkeit, Informationen (in Variablen) zu speichern, die auf mehreren SEiten verwendet werden sollen. 
Im Gegensatz zu einem Cookie werden die Informationen nicht auf dem Computer des Benutzers gespeichert. 

### Was ist eine PHP-Sitzung? 

Nimmt man Änderungen an einer Anwendung vor, so ist dies ähnlich einer Sitzung. Der PC weiß wer Sie sind, jedoch nicht der Webserver im Internet. Sitzungsvariablen lösen dieses Problem, indem sie Benutzerinformationen speichern, die auf mehreren Seiten verwendet werden (z.B.: Benutzername). 
Standardmäßig bleiben Sitzungvariablen bestehen, bis der Benutzer den Browser schließt. 

### Starten einer PHP-Sitzung 

Mit der Funktion session_start() wird eine Sitzung gestartet. 
Sitzungsvariablen werden mit der globalen PHP-Variable $_SESSIoN festgelegt. 
Die session_start() Funktion muss das allererste Element in dem Dokument sein. Noch vor allen HTML-Tags.

```php
//Start der Session
session_start(); 

//Setze Session-Variable 
$_SESSION["favcolor"] = "green";
$_SESSION["favanimal"] = "cat";
```

### Wert der PHP-Sitzungsvariablen abrufen 

Alle Werte der Sitzungsvariable werden in der globalen Variable $_SESSION gespeichert. 

```php 
//Session-Variablen welche auf der vorherigen Seite gesetzt werden. 
echo "Favorite color is " . $_SESSION["favcolor"] . ".<br>";
echo "Favorite animal is " . $_SESSION["favanimal"] . ".";
``` 

Eine weiter Möglichkeit wäre 
```php 
print_r($_SESSION);
```
### Ändern einer PHP-Sitzungsvariable 

Um eine Sitzungsvariable zu ändern, überschreiben Sie sie einfach. 

```php 
$_SESSION["favcolor"] = "yellow";
print_r($_SESSION); 
``` 
### Eine PHP-Sitzung zerstören 

Um alle globalen Sitzungsvariablen zu entfernen und die Sitzung zu zerstören, verwenden Sie session_unset() und session_destroy() 

```php 
// Entfernt die Session-Variable
session_unset();

// Zerstört die Session
session_destroy();
``` 
## Objektorientierung 

Objektorientierte Progammierung ist schneller und einfacher auszuführen. 

### Was ist Objektorientierte-Programmierung? 

Bei der objektorientierten Programmierung geht es darum, Objekte zu erstellen, welche Daten und Funktionen enthalten. 

Vorteile: 
- OOP ist schneller und einfacher auszuführen 
- OOP bietet eine klare Struktur für die Programme 
- OOP vermeidet Code-Duplikate und macht den Code einfacher zu warten, zu ändern und zu debuggen
- OOP ermöglicht die Erstellung vollständig wiederverwendenbarer Anwendungen mit weniger Code und kürzerer Entwicklungszeit.

### Was sind Klassen und Objekte? 

Klassen und Objekte sind die beiden Hauptaspekte der objektorientierten Programmierung. 

Eine Klasse ist also eine Vorlage für Objekte und ein Objekt ist eine Instanz einer Klasse. 
Wenn die einzelnen Objekte erstellt werden, erben sie alle Eigenschaften und Verhaltensweisen von der Klasse, aber jedes Objekt hat unterschiedliche Werte für die Eigenschaften. 

![Klassen&Objekte](images/Bild5.png) 

### Eine Klasse definierten 

Eine Klasse wird mit dem class Schlüsselwort definiert, gefolgt vom Namen der Klasse und alle seine Eigenschaften und Methoden befinden sich in den geschweiften Klammern. 

In einer Klasse heißen Variablen Eigenschaften und Funktionen Methoden! 

```php 
class Fruit {
  
}
``` 

### Objekte definieren 

Es können mehrere Objekte aus einer Klasse erstellt werden. Jedes Objekt hat alle in der Klasse definierten Eigenschaften und Methoden, aber sie haben unterschiedliche Eigenschaftswerte. 

Objekte werden mit dem new Schlüsselwort erstellt. 

```php 
class Fruit {
  // Properties
  public $name;
  public $color;

  // Methods
  function set_name($name) {
    $this->name = $name;
  }
  function get_name() {
    return $this->name;
  }
}

$apple = new Fruit();
$banana = new Fruit();
$apple->set_name('Apple');
$banana->set_name('Banana');

echo $apple->get_name();
echo "<br>";
echo $banana->get_name(); 
``` 
### Das Schlüsselwort $this 

Das Schlüsselwort $this bezieht sich auf das aktuelle Objekt und ist nur innerhalb von Methoden verfügbar. 

```php 
class Fruit {
  public $name;
  function set_name($name) {
    $this->name = $name;
  }
}
$apple = new Fruit();
$apple->set_name("Apple");

echo $apple->name; 
```

### Instanz von 

Mit dem instanceofSchlüsselwort kann man prüfen, ob ein Objekt zu einer bestimmten Klasse gehört. 

```php 
$apple = new Fruit();
var_dump($apple instanceof Fruit); 
``` 

### Konstruktor 

Der Konstruktor initialisiert beim erstellen eines Objektes dessen Eigenschaften. 
Zum erstellen eines Objektes aus einer Klasse verwendet man die Funktion __construct() 

```php 
class Fruit {
  public $name;
  public $color;

  function __construct($name) {
    $this->name = $name;
  }
  function get_name() {
    return $this->name;
  }
}

$apple = new Fruit("Apple");
echo $apple->get_name(); 
``` 
### Destruktor 

Ein Destruktor wird aufgerufen, wenn das Objekt zerstört oder das Skript gestoppt oder beendet wird. 

Wenn Sie eine __destruct()Funktion erstellen , ruft PHP diese Funktion automatisch am Ende des Skripts auf. 

```php 
class Fruit {
  public $name;
  public $color;

  function __construct($name) {
    $this->name = $name;
  }
  function __destruct() {
    echo "The fruit is {$this->name}.";
  }
}

$apple = new Fruit("Apple"); 
``` 

### Zugriffsmodifikatoren 

Es gibt drei Zugriffsmodifikatoren: 
- public - Auf die Eigenschaft bzw. Methode kann von überall zugegriffen werden. 
- protected - Auf die Eigenschaft bzw. Methode kann innerhalb der Klasse und dessen abgeleiteten Klassen zugegriffen werden. 
- private - Auf die Eigenschaft bzw. Methode kann NUR innerhalb der Klasse zugegriffen werden. 

```php 
class Fruit {
  public $name;
  protected $color;
  private $weight;
}

$mango = new Fruit();
$mango->name = 'Mango'; // OK
$mango->color = 'Yellow'; // ERROR
$mango->weight = '300'; // ERROR 
```

### Vererbung 

Wenn eine Klasse von einer anderen Klasse abstammt spricht man von Vererbung. 

Die untergeordnete Klasse erbt alle öffentlichen und geschützten Eigenschaften und Methoden von der übergeordneten Klasse. Darüber hinaus kann sie eigene Eigenschaften und Methoden haben. 

Eine geerbte Klasse wird mit dem extends Schlüsselwort definiert. 

```php 
class Strawberry extends Fruit {
  public function message() {
    echo "Am I a fruit or a berry? ";
  }  
    //Überschreiben von Methoden
  public function message(){  
    echo "Am I a fruit, a berry oder something else?";
  } 
``` 
Geerbte Methoden können überschrieben werden, indem die Methoden in der untergeordneten Klasse neu definiert werden (gleichen Namen verwenden). 

Das final Schlüsselwort kann verwendet werden, um die Klassenvererbung oder das Überschreiben von Methoden zu verhindern. 

```php 
final class Fruit {
  // Code
}

// Es wird ein Fehler auftreten
class Strawberry extends Fruit {
  // Code
} 
``` 

### Klassenkonstanten 

Konstanten können nach der Deklaration nicht mehr geändert werden.

Klassenkonstanten können nützlich sein, wenn Sie einige konstante Daten innerhalb einer Klasse definieren müssen. 

Eine Klassenkonstante wird innerhalb einer Klasse mit dem const Schlüsselwort deklariert. 

Wir können von außerhalb der Klasse auf eine Konstante zugreifen, indem wir den Klassennamen gefolgt vom Bereichsauflösungsoperator ( ::) gefolgt vom Konstantennamen verwenden. 

```php 
class Goodbye {
  const LEAVING_MESSAGE = "Thank you for visiting W3Schools.com!";
}

echo Goodbye::LEAVING_MESSAGE; 
```

Oder wir können von innerhalb der Klasse auf eine Konstante zugreifen, indem wir das selfSchlüsselwort gefolgt von dem Bereichsauflösungsoperator ( ::) gefolgt vom Konstantennamen verwenden. 

```php 
class Goodbye {
  const LEAVING_MESSAGE = "Thank you for visiting W3Schools.com!";
  public function byebye() {
    echo self::LEAVING_MESSAGE;
  }
} 
``` 
### Abstrakte Klassen 

Abstrakte Klassen und Methoden sind, wenn die Elternklasse eine benannte Methode hat, aber ihre Kindklasse(n) benötigt, um die Aufgaben zu erfüllen.

Eine abstrakte Klasse ist eine Klasse, die mindestens eine abstrakte Methode enthält. Eine abstrakte Methode ist eine Methode, die deklariert, aber nicht im Code implementiert ist.

Eine abstrakte Klasse oder Methode wird mit dem abstract Schlüsselwort definiert. 

```php 
abstract class ParentClass {
  abstract public function someMethod1();
  abstract public function someMethod2($name, $color);
  abstract public function someMethod3() : string;
} 
``` 

### Schnittstellen 

Mit Interfaces können Sie angeben, welche Methoden eine Klasse implementieren soll.

Schnittstellen machen es einfach, eine Vielzahl unterschiedlicher Klassen auf die gleiche Weise zu verwenden. Wenn eine oder mehrere Klassen dieselbe Schnittstelle verwenden, wird dies als "Polymorphismus" bezeichnet.

Schnittstellen werden mit dem **interface** Schlüsselwort deklariert. 

```php 
interface InterfaceName {
  public function someMethod1();
  public function someMethod2($name, $color);
  public function someMethod3() : string;
} 
``` 

### Schnittstellen verwenden 

Um eine Schnittstelle zu implementieren, muss eine Klasse das implementsSchlüsselwort verwenden.

Eine Klasse, die eine Schnittstelle implementiert, muss alle Methoden der Schnittstelle implementieren. 

```php 
interface Animal {
  public function makeSound();
}

class Cat implements Animal {
  public function makeSound() {
    echo "Meow";
  }
} 
``` 

### Was sind Eigenschaften? 

Traits werden verwendet, um Methoden zu deklarieren, die in mehreren Klassen verwendet werden können. Traits können Methoden und abstrakte Methoden haben, die in mehreren Klassen verwendet werden können, und die Methoden können einen beliebigen Zugriffsmodifizierer (öffentlich, privat oder geschützt) haben.

Eigenschaften werden mit dem **trait** Schlüsselwort deklariert. 

```php 
trait TraitName {
  // some code...
} 
``` 

### Statische Methoden 

Statische Methoden können direkt aufgerufen werden - ohne zuerst eine Instanz der Klasse zu erstellen. 

```php 
class ClassName {
  public static function staticMethod() {
    echo "Hello World!";
  }
} 
``` 

## JSON

JSON steht für Java Script Object Notation und ist ein leichtgewichtiges Datenaustauschformat. Es wird verwendet, um Daten zwischen Computern zu senden und ist sprachunabhängig. 

### Verwendung von JSON 

JavaScript hat eine eingebaute Funktion zum Konvertieren von JSON-Strings in JavaScript-Objekte. 

```json 
JSON.parse() 
```
JavaScript hat auch eine eingebaute Funktion zum Konvertieren eines Objekts in einen JSON-String. 

```json 
JSON.stringify() 
``` 

### Syntax 

Die JSON-Syntax wird von der JavaScript-Objektnotationssyntax abgeleitet:

- Daten sind in Name/Wert-Paaren
- Daten werden durch Kommas getrennt
- Geschweifte Klammern halten Gegenstände
- Eckige Klammern halten Arrays 

JSON-Daten werden als Name/Wert-Paare (auch bekannt als Schlüssel/Wert-Paare) geschrieben.

Ein Name/Wert-Paar besteht aus einem Feldnamen (in doppelten Anführungszeichen), gefolgt von einem Doppelpunkt, gefolgt von einem Wert. 

```json 
"name":"John" 
``` 
 
### Values 

In JSON müssen Werte einer der folgenden Datentypen sein:

- String
- Nummer
- Objekt
- Array
- Boolean
- null 

### JavaScript-Objekte 

Mit JavaScript köann man ein Objekt erstellen und diesem Daten zuweisen. 

```json 
person = {name:"John", age:31, city:"New York"}; 
``` 
